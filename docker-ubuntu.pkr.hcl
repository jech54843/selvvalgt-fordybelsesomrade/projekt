packer {
  required_plugins {
    docker = {
      version = ">= 0.0.7"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
  image  = "ubuntu:22.04"
  commit = true
}

build {
  name = "api"
  sources = [
    "source.docker.ubuntu"
  ]
  provisioner "shell-local" {
    command = "tar cf toupload/Api.tar build/Api"
  }
  provisioner "file" {
    destination = "/tmp/"
    source = "./toupload/"
  }
  provisioner "shell" {
    inline = [
      "apt-get update",
      "apt-get install -y aspnetcore-runtime-7.0",
      "tar xf /tmp/Api.tar"
    ]
  }
  post-processors {
    post-processor "docker-tag" {
      repository = "traware/api"
      tags       = ["latest"]
    }
  }
}

